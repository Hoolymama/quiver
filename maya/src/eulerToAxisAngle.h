#ifndef _eulerToAxisAngle
#define _eulerToAxisAngle


#include <maya/MBoundingBox.h>
#include <maya/MPxNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 
#include <maya/MPxLocatorNode.h>
 
class eulerToAxisAngle : public MPxNode
{
public:
						eulerToAxisAngle();
	virtual				~eulerToAxisAngle(); 

	virtual MStatus		compute( const MPlug& plug, MDataBlock& data );

	static  void*		creator();
	static  MStatus		initialize();
	
public:
	static  MObject aRotationX;
	static  MObject aRotationY;
	static  MObject aRotationZ;
	static  MObject aRotation;

	static  MObject	aRotateOrder;  


    static  MObject aOutputUnit;         
  
    static  MObject aAxisAngle;         

	static	MTypeId	id;
};

#endif
