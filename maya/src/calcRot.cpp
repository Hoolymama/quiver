
#include <maya/MIOStream.h>
#include <math.h>

#include <maya/MArgList.h>
#include <maya/MTypeId.h>

#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include <maya/MPlug.h>
#include <maya/MMatrix.h>
#include <maya/MVector.h>
#include <maya/MTransformationMatrix.h>
#include <maya/MQuaternion.h>
#include "calcRot.h"
#include "errorMacros.h"

#define ROTATE_ORDER_XYZ	0
#define ROTATE_ORDER_YZX	1
#define ROTATE_ORDER_ZXY	2
#define ROTATE_ORDER_XZY	3
#define ROTATE_ORDER_YXZ	4
#define ROTATE_ORDER_ZYX	5
#define OUTPUT_RADIANS	0
#define OUTPUT_DEGREES	1

#define X_AXIS	0
#define Y_AXIS	1
#define Z_AXIS	2

#define OUTPUT_EULER	0  // default
#define OUTPUT_ANGLE_AXIS	1
#define OUTPUT_MATRIX	2

#define RADIANS_TO_DEGREES  57.295779524

void* calcRot::creator()
{
	return new calcRot();
}

MSyntax calcRot::newSyntax()
{
	MSyntax syn;

	
	syn.addFlag(kFrontFlag, kFrontFlagL, MSyntax::kDouble, MSyntax::kDouble, MSyntax::kDouble);

	syn.addFlag(kUpFlag, kUpFlagL, MSyntax::kDouble, MSyntax::kDouble, MSyntax::kDouble);
	
	syn.addFlag(kFrontAxisFlag, kFrontAxisFlagL,  MSyntax::kLong);
	
	syn.addFlag(kUpAxisFlag, kUpAxisFlagL, MSyntax::kLong);
	
	syn.addFlag(kInvertFrontAxisFlag, kInvertFrontAxisFlagL);
	
	syn.addFlag(kInvertUpAxisFlag, kInvertUpAxisFlagL);
	
	syn.addFlag(kPhiFlag,	kPhiFlagL,	MSyntax::kDouble, MSyntax::kDouble, MSyntax::kDouble);

	syn.addFlag(kTranslateFlag,	kTranslateFlagL, MSyntax::kDouble, MSyntax::kDouble, MSyntax::kDouble);

	syn.addFlag(kScaleFlag,	kScaleFlagL,	MSyntax::kDouble, MSyntax::kDouble, MSyntax::kDouble);

	syn.addFlag(kOrderFlag, kOrderFlagL, MSyntax::kLong);
	
	syn.addFlag(kDegreesFlag, kDegreesFlagL, MSyntax::kBoolean);
	
	syn.addFlag(kInverseFlag, kInverseFlagL);
		
	syn.addFlag(kOutputFlag, kOutputFlagL, MSyntax::kLong);
	
	syn.enableQuery(false);
	syn.enableEdit(false);
	return syn;
		
 }

MStatus calcRot::redoIt(){return MS::kSuccess;}
MStatus calcRot::undoIt(){return MS::kSuccess;}
bool calcRot::isUndoable() const{return false;}

MStatus calcRot::doIt( const MArgList& args )
{
	//cerr << "DOING calcRot"<< endl;
	MStatus st;

	MString method("calcRot::doIt");

	// MSpace::Space	space=MSpace::kWorld;
	
	MArgDatabase  argData(syntax(), args);

	// required args
	////////////////////////////////////////////////////
	// if x is set but not y - then 
	
	
	if ( ! ((  argData.isFlagSet(kFrontFlag) && argData.isFlagSet(kUpFlag)) ||  argData.isFlagSet(kPhiFlag)) )
	{ 
		displayError("expected either (-xAxis and -yAxis flags, or -phi flag");
		return MS::kFailure;
	}

	
	MVector translate(MVector::zero);
	MVector scale(1.0,1.0,1.0);
	MVector front;
	MVector up;
	MVector phi;

	
	// double xx, xy, xz, yx, yy, yz;
	int order=ROTATE_ORDER_XYZ;
	bool deg=OUTPUT_DEGREES;
	bool inv=0;	
	int out=OUTPUT_EULER;
	
	int frontAxis = X_AXIS;
	int upAxis = Y_AXIS;


	
	if (argData.isFlagSet(kFrontFlag)) {
		st= argData.getFlagArgument (kFrontFlag, 0, front.x);er;
		st= argData.getFlagArgument (kFrontFlag, 1, front.y);er;
		st= argData.getFlagArgument (kFrontFlag, 2, front.z);er;
		
		if (argData.isFlagSet(kInvertFrontAxisFlag)) {
			front.x = -front.x;
			front.y = -front.y;
			front.z = -front.z;
		}	
		
		
		st= argData.getFlagArgument (kUpFlag, 0, up.x);er;
		st= argData.getFlagArgument (kUpFlag, 1, up.y);er;
		st= argData.getFlagArgument (kUpFlag, 2, up.z);er;
		
		if (argData.isFlagSet(kInvertUpAxisFlag)) {
			up.x = -up.x;
			up.y = -up.y;
			up.z = -up.z;
		}
		////////////////////////////////////////////////////
		
	} else { // phi must be set
		st= argData.getFlagArgument (kPhiFlag, 0, phi.x);er;
		st= argData.getFlagArgument (kPhiFlag, 1, phi.y);er;
		st= argData.getFlagArgument (kPhiFlag, 2, phi.z);er;
	}
	
	if (argData.isFlagSet(kTranslateFlag)) {
		st= argData.getFlagArgument (kTranslateFlag, 0, translate.x);er;
		st= argData.getFlagArgument (kTranslateFlag, 1, translate.y);er;
		st= argData.getFlagArgument (kTranslateFlag, 2, translate.z);er;
	}
	if (argData.isFlagSet(kScaleFlag)) {
		st= argData.getFlagArgument (kScaleFlag, 0, scale.x);er;
		st= argData.getFlagArgument (kScaleFlag, 1, scale.y);er;
		st= argData.getFlagArgument (kScaleFlag, 2, scale.z);er;
	}
	
	if (argData.isFlagSet(kOrderFlag)) {
		st= argData.getFlagArgument (kOrderFlag, 0, order);er;
	}
	if (argData.isFlagSet(kDegreesFlag)) {
		st= argData.getFlagArgument (kDegreesFlag, 0, deg);er;
	}
	if (argData.isFlagSet(kInverseFlag)) {
		inv = 1;
	}
	if (argData.isFlagSet(kOutputFlag)) {
		st= argData.getFlagArgument (kOutputFlag, 0, out);er;
	}
	
	if (argData.isFlagSet(kFrontAxisFlag)) {
		st= argData.getFlagArgument (kFrontAxisFlag, 0, frontAxis);er;
	}	
	if (argData.isFlagSet(kUpAxisFlag)) {
		st= argData.getFlagArgument (kUpAxisFlag, 0, upAxis);er;
	}		

	////////////////////////////////////////////////////
	/*
	if ( !( (  argData.isFlagSet(kScaleFlag) || argData.isFlagSet(kTranslateFlag)) && (out != OUTPUT_MATRIX) ))
	{ 
		displayWarning("calcRot ignores the scale and translate flags unless output is set to matrix (2)");
	}
	*/
	

	// rotate order
	////////////////////////////////////////////////////
	MTransformationMatrix::RotationOrder ord;
	switch ( order ) {
		case ROTATE_ORDER_XYZ:
			ord = MTransformationMatrix::kXYZ; break;
		case ROTATE_ORDER_YZX:
			ord = MTransformationMatrix::kYZX; break;
		case ROTATE_ORDER_ZXY:
			ord = MTransformationMatrix::kZXY; break;
		case ROTATE_ORDER_XZY:
			ord = MTransformationMatrix::kXZY; break;
		case ROTATE_ORDER_YXZ:
			ord = MTransformationMatrix::kYXZ; break;
		case ROTATE_ORDER_ZYX:
			ord = MTransformationMatrix::kZYX; break;
		default:
			ord = MTransformationMatrix::kXYZ; break;
	}
	////////////////////////////////////////////////////
	
	// now we have all the info we need.
	if (argData.isFlagSet(kFrontFlag) ) { // aim and up
		st = calcResult(front,up,translate,scale,order,deg,inv,frontAxis,upAxis,out);
	} else {
		st = calcResult(phi,translate,scale,order,deg,inv,out);
	}
	
	
	
	
	return MS::kSuccess;
}



MStatus calcRot::calcResult(const MVector &phi,const MVector &trans,const MVector &scale,int order,bool deg,bool inv,int out) const {
	// phi
	
	MVector ax(phi);
	double theta = ax.length();
	if (theta==0.0)  {
		ax = MVector::yAxis;
	} else {
		ax.normalize();
	}

	
	if (out == OUTPUT_ANGLE_AXIS) {
		
		if (deg) {
			theta *= RADIANS_TO_DEGREES;
		}
		
		if (inv) theta = -theta;

		appendToResult(theta);
		appendToResult(ax.x);
		appendToResult(ax.y);
		appendToResult(ax.z);
		return MS::kSuccess;
	}
	
	MQuaternion q(theta,ax);
	
	MTransformationMatrix mtMat(MMatrix::identity);
	mtMat.setRotationQuaternion(q.x,q.y,q.z,q.w);
	
	
	
	if (out == OUTPUT_EULER) {
		
		
		
		// rotate order
		////////////////////////////////////////////////////
		MTransformationMatrix::RotationOrder ord;
		switch ( order ) {
			case ROTATE_ORDER_XYZ:
				ord = MTransformationMatrix::kXYZ; break;
			case ROTATE_ORDER_YZX:
				ord = MTransformationMatrix::kYZX; break;
			case ROTATE_ORDER_ZXY:
				ord = MTransformationMatrix::kZXY; break;
			case ROTATE_ORDER_XZY:
				ord = MTransformationMatrix::kXZY; break;
			case ROTATE_ORDER_YXZ:
				ord = MTransformationMatrix::kYXZ; break;
			case ROTATE_ORDER_ZYX:
				ord = MTransformationMatrix::kZYX; break;
			default:
				ord = MTransformationMatrix::kXYZ; break;
		}
		////////////////////////////////////////////////////
		
		if (inv) {
			mtMat = MTransformationMatrix(mtMat.asMatrixInverse());
		}
		
		
		double rotValue[3];
		mtMat.getRotation( rotValue, ord );
		////////////////////////////////////////////////////
		
		if (deg) {
			rotValue[0] *= RADIANS_TO_DEGREES;
			rotValue[1] *= RADIANS_TO_DEGREES;
			rotValue[2] *= RADIANS_TO_DEGREES;
		}
		appendToResult(rotValue[0]);
		appendToResult(rotValue[1]);
		appendToResult(rotValue[2]);
		////////////////////////////////////////////////////
		return MS::kSuccess;
	}
	
	
	if (out == OUTPUT_MATRIX) {
		
		
		
		const double dScale[3] = {scale.x,scale.y,scale.z};

		mtMat.setScale(dScale , MSpace::kTransform);
		mtMat.setTranslation(trans, MSpace::kTransform)	;
		MMatrix mmat;
		if (inv) {
			mmat = mtMat.asMatrixInverse();
		} else {
			mmat = mtMat.asMatrix();
		}
		

		appendToResult(mmat[0][0]);	appendToResult(mmat[0][1]);	appendToResult(mmat[0][2]);	appendToResult(mmat[0][3]);
		appendToResult(mmat[1][0]);	appendToResult(mmat[1][1]);	appendToResult(mmat[1][2]);	appendToResult(mmat[1][3]);
		appendToResult(mmat[2][0]);	appendToResult(mmat[2][1]);	appendToResult(mmat[2][2]);	appendToResult(mmat[2][3]);
		appendToResult(mmat[3][0]);	appendToResult(mmat[3][1]);	appendToResult(mmat[3][2]);	appendToResult(mmat[3][3]);
		return MS::kSuccess;
	}
	
	return MS::kSuccess;

}
	








MStatus calcRot::calcResult(const MVector &front,const MVector &up, const MVector &trans,const MVector &scale,int order,bool deg,bool inv,int frontAxis,int upAxis,int out) const {
	
	
	double mat[4][4] ;
	MVector fAxis = front.normal();
	MVector uAxis(up);
	
	// make sure vectors are valid and prepare orthonormal xyz
	////////////////////////////////////////////////////
	if (fAxis.isEquivalent(MVector::zero)) {
		fAxis = (MVector::xAxis);
	}
	if (uAxis.isEquivalent(MVector::zero)) {
		uAxis = (MVector::yAxis);
	}
	if  ( fAxis.isParallel(uAxis) ) { // gotta do something, 
		if ( fAxis.isParallel(MVector::yAxis) ) {
			uAxis = (MVector::xAxis) ;
		} else {
			uAxis = (MVector::yAxis);	
		}
	}
	MVector sAxis = (fAxis^uAxis).normal(); 
	uAxis = (sAxis^fAxis).normal(); 
	////////////////////////////////////////////////////
	
	MVector xa, ya, za;
	
	
	if (frontAxis == X_AXIS)	{  
		xa = fAxis;
		if (upAxis == Y_AXIS ) {
			ya = uAxis;
			za = sAxis;
		} else {
			za = uAxis;
			ya = -sAxis;
		}
	} else if (frontAxis == Y_AXIS)	{  
		ya = fAxis;
		if (upAxis == Z_AXIS ) {
			za = uAxis;
			xa = sAxis;
		} else {
			xa = uAxis;
			za = -sAxis;
		}
	} else if (frontAxis == Z_AXIS)	{  
		za = fAxis;
		if (upAxis == X_AXIS ) {
			xa = uAxis;
			ya = sAxis;
		} else {
			ya = uAxis;
			xa = -sAxis;
		}
	}						
	
	
	// fill in the rot matrix
	////////////////////////////////////////////////////
	mat[0][0] = xa.x;	mat[0][1] = xa.y;	mat[0][2] = xa.z;	mat[0][3] = 0;
	mat[1][0] = ya.x;	mat[1][1] = ya.y;	mat[1][2] = ya.z;	mat[1][3] = 0;
	mat[2][0] = za.x;	mat[2][1] = za.y;	mat[2][2] = za.z;	mat[2][3] = 0;
	mat[3][0] = 0.0;		mat[3][1] = 0.0;		mat[3][2] = 0.0;		mat[3][3] = 1.0;
	
	MMatrix mmat(mat);
	


	

	
	if (out == OUTPUT_MATRIX) {
		
		const double dScale[3] = {scale.x,scale.y,scale.z};
		
		MTransformationMatrix mtMat(mmat);
		mtMat.setScale(dScale, MSpace::kTransform);
		mtMat.setTranslation(trans, MSpace::kTransform)	;
		
		if (inv) {
			mmat = mtMat.asMatrixInverse();
		} else {
			mmat = mtMat.asMatrix();
		}
		
		appendToResult(mmat[0][0]);	appendToResult(mmat[0][1]);	appendToResult(mmat[0][2]);	appendToResult(mmat[0][3]);
		appendToResult(mmat[1][0]);	appendToResult(mmat[1][1]);	appendToResult(mmat[1][2]);	appendToResult(mmat[1][3]);
		appendToResult(mmat[2][0]);	appendToResult(mmat[2][1]);	appendToResult(mmat[2][2]);	appendToResult(mmat[2][3]);
		appendToResult(mmat[3][0]);	appendToResult(mmat[3][1]);	appendToResult(mmat[3][2]);	appendToResult(mmat[3][3]);
		return MS::kSuccess;
	}
	
	
	
	if (out == OUTPUT_EULER) {
		
		
		
		// rotate order
		////////////////////////////////////////////////////
		MTransformationMatrix::RotationOrder ord;
		switch ( order ) {
			case ROTATE_ORDER_XYZ:
				ord = MTransformationMatrix::kXYZ; break;
			case ROTATE_ORDER_YZX:
				ord = MTransformationMatrix::kYZX; break;
			case ROTATE_ORDER_ZXY:
				ord = MTransformationMatrix::kZXY; break;
			case ROTATE_ORDER_XZY:
				ord = MTransformationMatrix::kXZY; break;
			case ROTATE_ORDER_YXZ:
				ord = MTransformationMatrix::kYXZ; break;
			case ROTATE_ORDER_ZYX:
				ord = MTransformationMatrix::kZYX; break;
			default:
				ord = MTransformationMatrix::kXYZ; break;
		}
		////////////////////////////////////////////////////
		
		if (inv) mmat = mmat.inverse();
		MTransformationMatrix mtMat(mmat);
		

		double rotValue[3];
		mtMat.getRotation( rotValue, ord );
		////////////////////////////////////////////////////

		if (deg) {
			rotValue[0] *= RADIANS_TO_DEGREES;
			rotValue[1] *= RADIANS_TO_DEGREES;
			rotValue[2] *= RADIANS_TO_DEGREES;
		}
		appendToResult(rotValue[0]);
		appendToResult(rotValue[1]);
		appendToResult(rotValue[2]);
		////////////////////////////////////////////////////
		return MS::kSuccess;
	}
	
	
	
	
	if (out == OUTPUT_ANGLE_AXIS) {
		
		MVector ax;
		double theta;
		MQuaternion q;
		q = mmat;
		q.getAxisAngle(ax,theta) ;
		
		if (deg) {
			theta *= RADIANS_TO_DEGREES;
		}
		if (inv) theta = -theta;
		appendToResult(theta);
		appendToResult(ax.x);
		appendToResult(ax.y);
		appendToResult(ax.z);
		return MS::kSuccess;
	}
	return MS::kSuccess;
	
	
}


