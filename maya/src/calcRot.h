#ifndef	__calcRot_H__
#define	__calcRot_H__

#include <maya/MPxCommand.h>


#define kFrontFlag				"-fv"
#define kFrontFlagL				"-frontVector"
#define kUpFlag					"-uv"
#define kUpFlagL				"-upVector"

#define kFrontAxisFlag			"-fa"
#define kFrontAxisFlagL			"-frontAxis"
#define kUpAxisFlag				"-ua"
#define kUpAxisFlagL			"-upAxis"

#define kInvertFrontAxisFlag	"-if"
#define kInvertFrontAxisFlagL 	"-invertFrontAxis"
#define kInvertUpAxisFlag		"-iu"
#define kInvertUpAxisFlagL		"-invertUpAxis"

#define kPhiFlag				"-p"
#define kPhiFlagL				"-phiRotation"
#define kOrderFlag				"-o"
#define kOrderFlagL				"-order"
#define kDegreesFlag			"-d"
#define kDegreesFlagL			"-degrees"
#define kInverseFlag			"-i"
#define kInverseFlagL			"-inverse"
#define kTranslateFlag			"-t"
#define kTranslateFlagL			"-translate"
#define kScaleFlag				"-s"
#define kScaleFlagL				"-scale"
#define kOutputFlag				"-u"
#define kOutputFlagL			"-output"

class MPoint;
class MVector;

/////////////////////////////////////////
//
//	calcRot
//		: MPxCommand
//
class calcRot : public MPxCommand
{
	public:
	
	calcRot() {}
	virtual ~calcRot() {}
	
	MStatus doIt( const MArgList& args );
	MStatus		redoIt();
	MStatus		undoIt();
	bool		isUndoable() const;
	MStatus calcResult(const MVector &front,const MVector &up, const MVector &trans,const MVector &scale,int order,bool deg,bool inv,int frontAxis,int upAxis,int out) const;
	MStatus calcResult(const MVector &phi,const MVector &trans,const MVector &scale,int order,bool deg,bool inv,int out) const;

	
	static void* creator();
	
   static MSyntax      newSyntax();	

};

#endif	//	!__calcRot_H__
