#ifndef	__curveQuery_H__
#define	__curveQuery_H__

#include <maya/MPxCommand.h>
#include <maya/MDagPath.h>
#include <maya/MPoint.h>



#define kClosestFlag    "-cp"
#define kClosestFlagL 	"-closestPoint"
#define kDistanceFlag    "-d"
#define kDistanceFlagL   "-distance"
#define kPointFlag    "-p"
#define kPointFlagL   "-point"

#define kFractionFlag    "-f"
#define kFractionFlagL   "-fraction"
#define kParameterFlag   "-r"
#define kParameterFlagL   "-parameter"

#define kHelpFlag       "-h"
#define kHelpFlagL       "-help"

#define CLOSEST     (1 << 0)
#define DISTANCE    (1 << 1)
#define PARAMETER   (1 << 2)

// wtf

/////////////////////////////////////////
//
//	curveQuery
//		: MPxCommand
//
class curveQuery : public MPxCommand
{
	public:
	
	curveQuery() {}
	virtual ~curveQuery() {}
	
	MStatus doIt( const MArgList& args );
	
	static void* creator();
	
  static MSyntax      newSyntax();	

	protected:

	MStatus getClosestPoint(const MDagPath &dagPath, const MPoint &testPoint, short vals)	const;
	MStatus getFraction(const MDagPath &dagPath, double fraction, bool returnAPoint) const;
};

#endif	//	!__curveQuery_H__
