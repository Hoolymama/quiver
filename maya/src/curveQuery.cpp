#include <maya/MIOStream.h>
#include <math.h>

#include <maya/MArgList.h>
#include <maya/MTypeId.h>
#include <maya/MSelectionList.h>
#include <maya/MItSelectionList.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MDagPath.h>
#include <maya/MObject.h>
// #include <maya/MFnPlugin.h>
#include <maya/MVector.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>


#include "curveQuery.h"
#include "errorMacros.h"

#define EPSILON 0.0001


//	static
void* curveQuery::creator()
{
    return new curveQuery();
}

MSyntax curveQuery::newSyntax()
{
    MSyntax syn;

    syn.addFlag(kClosestFlag, kClosestFlagL);
    syn.addFlag(kDistanceFlag, kDistanceFlagL);
    syn.addFlag(kPointFlag, kPointFlagL, MSyntax::kDouble, MSyntax::kDouble, MSyntax::kDouble);
    syn.addFlag(kFractionFlag, kFractionFlagL, MSyntax::kDouble);
    syn.addFlag(kParameterFlag, kParameterFlagL);

    syn.addFlag(kHelpFlag, kHelpFlagL);

    syn.setObjectType(MSyntax::kSelectionList, 1, 1);
    syn.useSelectionAsDefault(true);

    syn.enableQuery(false);
    syn.enableEdit(false);
    return syn;
}

MStatus curveQuery::doIt( const MArgList& args )
{
    MStatus st;
   //unsigned int counter = 0;
    MString method("curveQuery::doIt");
    MSelectionList list;

    MArgDatabase argData(syntax(), args);
    if (argData.isFlagSet(kHelpFlag))
    {
        MString helpString("curveQuery [-point|-fraction] [flags] <object>\n");
        helpString += "\t-p/-point  float float float \t\tthe test point\n";
        helpString += "\t-cp/-closestPoint\t\t\treturn the closest point to this point\n";
        helpString += "\t-d/-distance \t\t\t\treturn distance to the closest point\n";
        helpString += "\t-f/-fraction  float\t\t\tthe point at this fraction of the curve'st length\n";
        helpString += "\t-r/-parameter \t\t\t\tuse with -f to return the parameter at this fraction of the curve'st length\n";
        helpString += "\t\t\t\t\t\tor -p to obtain the parameter at the closest point\n";
        helpString += "\t-h/-help\n\n";
        helpString += "When run in -p mode, queried quantities are returned\n";
        helpString += "in the order closestPoint, distance, parameter\n\n";
        helpString += "Examples:\n";
        helpString += "curveQuery -p 1.2 0.3 4.5 -cp -d curve1\n";
        helpString += "curveQuery -f 0.2 -r curve1\n";

        setResult(helpString);
        return MS::kSuccess;
    }
    argData.getObjects(list);
    MDagPath dagPath;

    // check that the object is a curve
    MItSelectionList iter( list, MFn::kNurbsCurve);
    if (iter.isDone())
    {
        displayError("Object must be a curve");
        return MS::kUnknownParameter;
    }

    // get the path
    st = iter.getDagPath( dagPath ); ert;

    if (argData.isFlagSet(kPointFlag))
    {
        MPoint point;
        st = argData.getFlagArgument (kPointFlag, 0, point.x); ert;
        st = argData.getFlagArgument (kPointFlag, 1, point.y); ert;
        st = argData.getFlagArgument (kPointFlag, 2, point.z); ert;
				
        short vals = 0;
				
        if (argData.isFlagSet(kDistanceFlag))
        {
            vals |= DISTANCE;
        }
				
        if (argData.isFlagSet(kClosestFlag))
        {
            vals |= CLOSEST;
        }
				
        if (argData.isFlagSet(kParameterFlag))
        {
            vals |= PARAMETER;
        }

        if (vals == 0)
        {
            displayError("Must request either " kDistanceFlagL ", " kClosestFlagL " , or " kParameterFlagL 
								         " when using " kPointFlagL);
            return MS::kFailure;
        }

        st = getClosestPoint(dagPath, point, vals); er;
    }
    else if (argData.isFlagSet(kFractionFlag))
    {
        double f;
        st = argData.getFlagArgument (kFractionFlag, 0, f); 
				
				if (!st || f < 0.0 || f > 1.0)
				{
					displayError("Must specify floating point value between 0.0 and 1.0 with " kFractionFlagL);
					return MS::kFailure;
				}
				
        if (argData.isFlagSet(kParameterFlag))
        {
            st = getFraction(dagPath, f, false); er;
        }
        else
        {
            st = getFraction(dagPath, f, true); er;
        }
    }
    else
    {
        displayError("Must specify either " kPointFlagL " or " kFractionFlagL);
        return MS::kFailure;
    }

    return st;
}

MStatus curveQuery::getFraction(const MDagPath & dagPath , double fraction, bool returnAPoint) const
{
    MStatus st;
  //  unsigned int counter = 0;
    MString method("curveQuery::getFraction");
		
    
	if (fraction < 0.0) fraction = 0.0;
	if (fraction > 1.0) fraction = 1.0;
	
    double param = 0.0;
    MFnNurbsCurve curveFn(dagPath);
    double arclen = curveFn.length(EPSILON, &st); ert;
    double fracLen = fraction * arclen;
    param = curveFn.findParamFromLength(fracLen, &st); ert;

    if (returnAPoint)
    {
        MPoint resultPoint;
        st = curveFn.getPointAtParam(param,resultPoint, MSpace::kWorld); ert;

        appendToResult(resultPoint.x);
        appendToResult(resultPoint.y);
        appendToResult(resultPoint.z);
    }
    else
    {
        setResult(param);
    }
		
    return MS::kSuccess;
}

MStatus curveQuery::getClosestPoint(const MDagPath & dagPath , const MPoint &testPoint, short vals) const
{
    MStatus st;
    //unsigned int counter = 0;
    MString method("curveQuery::getClosestPoint");

    double param = 0;
    MFnNurbsCurve curveFn(dagPath);
    MPoint resultPoint = curveFn.closestPoint(testPoint, &param ,EPSILON, MSpace::kWorld, &st); ert;

    if (vals & CLOSEST)
    {
        appendToResult(resultPoint.x);
        appendToResult(resultPoint.y);
        appendToResult(resultPoint.z);
    }

    if (vals & DISTANCE)
    {
        MVector diff = (testPoint - resultPoint);
        double distance	= diff.length();
        appendToResult(distance);
    }

    if (vals & PARAMETER)
    {
        appendToResult(param);
    }

    return MS::kSuccess;
}

