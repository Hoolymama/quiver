

#include <maya/MStatus.h>
#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>
#include "errorMacros.h"
#include "curveQuery.h"
#include "eulerToAxisAngle.h"
#include "calcRot.h"



MStatus initializePlugin( MObject obj )
{
	
	MStatus st;
	
	MString method("initializePlugin");
	
	 MFnPlugin plugin( obj, PLUGIN_VENDOR, PLUGIN_VERSION , MAYA_VERSION);

	

	 st = plugin.registerCommand( "curveQuery",  curveQuery::creator ,  curveQuery::newSyntax);ert;
	 st = plugin.registerCommand( "calcRot",  calcRot::creator ,  calcRot::newSyntax);ert;
	 st = plugin.registerNode( "eulerToAxisAngle", eulerToAxisAngle::id, eulerToAxisAngle::creator, eulerToAxisAngle::initialize );er;

	 MGlobal::executePythonCommand("import quiver;quiver.load()");

	 return st;

}

MStatus uninitializePlugin( MObject obj)
{
	MStatus st;
	
	MString method("uninitializePlugin");
	
	MFnPlugin plugin( obj );
	st = plugin.deregisterNode( eulerToAxisAngle::id );ert;

	st = plugin.deregisterCommand( "calcRot" );ert;

	st = plugin.deregisterCommand( "curveQuery" );ert;
	
	
	
	return st;
}



