
#include <maya/MIOStream.h>

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MVector.h>
#include <maya/MFloatVector.h>
#include <maya/MFloatVectorArray.h>

#include <maya/MColorArray.h>
#include <maya/MFloatArray.h>
#include <maya/MQuaternion.h>
#include <maya/MEulerRotation.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnUnitAttribute.h>

#include <maya/MFnNumericAttribute.h>
#include <maya/MFloatVector.h>
#include <maya/MPoint.h>

#include <maya/MAngle.h>
#include <maya/MBoundingBox.h>
#include "errorMacros.h"
#include "jMayaIds.h"
#include "mayaMath.h"

#include "eulerToAxisAngle.h"


#include <maya/MGlobal.h>

MTypeId     eulerToAxisAngle::id( k_eulerToAxisAngle );


const double rad_to_deg = (180 / 3.1415927);

MObject     eulerToAxisAngle::aRotationX;
MObject     eulerToAxisAngle::aRotationY;
MObject     eulerToAxisAngle::aRotationZ;
MObject     eulerToAxisAngle::aRotation;

MObject     eulerToAxisAngle::aRotateOrder;  

MObject     eulerToAxisAngle::aOutputUnit;         
MObject     eulerToAxisAngle::aAxisAngle; 



eulerToAxisAngle::eulerToAxisAngle() {}
eulerToAxisAngle::~eulerToAxisAngle() {}

MStatus eulerToAxisAngle::compute( const MPlug& plug, MDataBlock& data )
{

	MStatus st;
	cerr << "plug name" << plug.name() << endl;
	MPlug thePlug = plug;
	if (plug.isChild()) thePlug = plug.parent();
	if ( !(
		(thePlug == aAxisAngle) )  ) return MS::kUnknownParameter;

	double rotValue[3];
	rotValue[0] = data.inputValue(aRotationX).asAngle().asRadians();
	rotValue[1] = data.inputValue(aRotationY).asAngle().asRadians();
	rotValue[2] = data.inputValue(aRotationZ).asAngle().asRadians();

	
	mayaMath::RotateOrder rotateOrder = mayaMath::RotateOrder(data.inputValue(aRotateOrder).asShort());
	mayaMath::RotateUnit outUnit = mayaMath::RotateUnit(data.inputValue(aOutputUnit).asShort());

	MTransformationMatrix::RotationOrder ord;
	switch ( rotateOrder ) {
		case mayaMath::xyz:
		ord = MTransformationMatrix::kXYZ; break;
		case mayaMath::yzx:
		ord = MTransformationMatrix::kYZX; break;
		case mayaMath::zxy:
		ord = MTransformationMatrix::kZXY; break;
		case mayaMath::xzy:
		ord = MTransformationMatrix::kXZY; break;
		case mayaMath::yxz:
		ord = MTransformationMatrix::kYXZ; break;
		case mayaMath::zyx:
		ord = MTransformationMatrix::kZYX; break;
		default:
		ord = MTransformationMatrix::kInvalid; break;
	}


	MTransformationMatrix mtMat;
	mtMat.setRotation( rotValue, ord );

	MVector out = MVector::zero;
	MQuaternion q = mtMat.rotation();
	MVector axis;
	double theta;
	bool hasAngle = q.getAxisAngle(axis, theta);
	if (hasAngle) {
		out = axis.normal() * theta;
	}
	if (outUnit == mayaMath::kDegrees) {
		out *= rad_to_deg;
	}
	// cerr << "out: "  << out << endl; 

	MDataHandle hOut = data.outputValue(aAxisAngle);
	MFloatVector& result = hOut.asFloatVector();

	result.x = out.x;
	result.y = out.y;
	result.z = out.z;

	data.setClean(aAxisAngle);

	return MS::kSuccess;
}



void* eulerToAxisAngle::creator()

{
	return new eulerToAxisAngle();
}



MStatus eulerToAxisAngle::initialize()
{

	MStatus	st;
	MString method("eulerToAxisAngle::initialize");
	MFnUnitAttribute uAttr;
	MFnEnumAttribute eAttr;
	MFnNumericAttribute nAttr;




  	aRotationX = uAttr.create("rotationX","rox", MFnUnitAttribute::kAngle);
	aRotationY = uAttr.create("rotationY","roy", MFnUnitAttribute::kAngle);
	aRotationZ = uAttr.create("rotationZ","roz", MFnUnitAttribute::kAngle);
	aRotation = nAttr.create("rotation","rot",aRotationX, aRotationY, aRotationZ);
	st = addAttribute( aRotation );er;



	aRotateOrder = eAttr.create( "rotateOrder", "ro", mayaMath::xyz);
	eAttr.addField("xyz", mayaMath::xyz);
	eAttr.addField("yzx", mayaMath::yzx);
	eAttr.addField("zxy", mayaMath::zxy);
	eAttr.addField("xzy", mayaMath::xzy);
	eAttr.addField("yxz", mayaMath::yxz);
	eAttr.addField("zyx", mayaMath::zyx);
	eAttr.setKeyable(true);
	eAttr.setHidden(false);
	st = addAttribute( aRotateOrder );er;


	aOutputUnit = eAttr.create( "angularUnit", "ang", mayaMath::kRadians);
	eAttr.addField("radians", mayaMath::kRadians);
	eAttr.addField("degrees",mayaMath::kDegrees);
	eAttr.setKeyable(true);
	eAttr.setHidden(false);
	st = addAttribute( aOutputUnit );er;


	aAxisAngle  = nAttr.createPoint( "axisAngle", "axa");
	nAttr.setStorable(false);
	nAttr.setReadable(true);
	nAttr.setHidden(false);
	st = addAttribute( aAxisAngle );er;


	st = attributeAffects( aRotationX, aAxisAngle );er;
	st = attributeAffects( aRotationY, aAxisAngle );er;
	st = attributeAffects( aRotationZ, aAxisAngle );er;
	st = attributeAffects( aRotation, aAxisAngle );er;
	st = attributeAffects( aRotateOrder, aAxisAngle );er;
	st = attributeAffects( aOutputUnit, aAxisAngle );er;

	return MS::kSuccess;

}




